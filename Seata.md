- Metadata
	- Author: superrick
	- lilnk:
	- Publisher:
	- Date: [[2022-04-29]]
	- Status: 
	- Tag: #seata #分布式 #事务
- content
### AT模式（默认模式）（中间件）
提供无侵入自动补偿的事物模式
1. 需要支持本地事务的数据库
2. 事物协调者 - TC (tansaction Coordinator)

### XA模式（两阶段提交）(数据库)
支持已实现 XA 接口的数据库的XA模式
1. 数据库需要支持 （XA）
```
show engines;

show variables like 'innodb_support_xa';
```

### TCC模式（应用层）
1. 支持TCC 模式并可与AT混用，灵活度更高
2. （try, confirm, cancel）
3. TCC 会增加很多代码量，所有逻辑需要自定义函数

![[Screenshot 2022-04-29_11-48-37-061.png]]

### SAGA模式
为长事务提供有效的解决方案
1. 补偿事务，或者重试直到成功
2. 不支持 ACID 保证
![[Screenshot 2022-04-29_13-20-26-185.png]]

### 消息机制
1. 流程
	1. 执行业务逻辑
	2. 发送解藕的消息
2. 方案
	1. rocketmq 事务消息
	2. 

### Seata 框架的角色
1. TC (Transaction Coordinator) 事务协调者
	1. 维护全局和分支事物的状态，驱动全局事物提交或回滚
2. TM (Transaction Manager) 事务管理员
	1. 定义全局事物的范围，开始全局事务，提交或回滚全局事物
3. Rm (Resource Manager) 资源管理员
	1. 管理分支事务处理的资源，与TC交互以注册分支事务和报告分支事务的状态，并驱动分支事务提交或回滚


![[Screenshot 2022-04-29_13-55-06-977.png]]