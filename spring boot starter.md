- Metadata
	- Author: superrick
	- lilnk:
	- Publisher:
	- Date: [[2022-04-28]]
	- Status: 
	- Tag: #springboot #starter
- content

### 1. 需要在 resources 下创建 META-INF/spring.factories 文件
```
org.springframework.boot.autoconfigure.EnableAutoConfiguration=com.spring.study.HelloSerivceAutoConfiguration
```
### 2. 其他工程 将此工程引入 maven 就可以自动使用
### 3. 命名规范
spring 官方
	spring-boot-starter-模块名称
自定义
	模块名-spring-boot-starter
### 4. 手写 spring boot starter
1. 定义配置类 RedissonProperties
```
@ConfigurationProperties(prefix = "gp.redisson")
public class RedissonProperties {

	private String host = "localhost";
	private int port = 6379;
}
```
2. 定义 RedissonAutoConfiguration
```
@Configuration
@ConditionalOnClass(Redisson.class)
@EnableConfigurationProperties(RedissonProperties.class)
public class redissonAutoConfiguration {

	@Bean
	RedissonClient redissonClient(RedissonProterties redissonProperties) {
		... ...
	}
}
```
3. 在resource下创建 META-INF/spring.factories
```
org.springframework.boot.autoconfigure.EnableAutoConfiguration=\
com.gupaoedu.RedissonAutoConfiguration
```
4. 引入并使用，引入maven 设置配置项目
```
gp.redisson.host=192.168.13.106
gp.redisson.port=6379
```