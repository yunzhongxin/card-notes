- Metadata
	- Author: superrick
	- lilnk:
	- Publisher:
	- Date: [[2022-04-29]]
	- Status: 
	- Tag: #zookeeper #分布式
- content
[[zookeeper 安装]]

### 用途
1. 服务注册中心
	1. 使用临时节点，会话结束后临时节点删除
2. 数据发布订阅，配置中心
	1. watcher 特性，可以收到变更事件
3. 命名服务
4. 集群管理
5. 分布式锁

### 特性
1. 顺序一致性
2. 原子性
3. 单一视图
4. 可靠性
5. 实时性

### 算法
1. Paxos 算法
2. Zab 协议
	1. 消息广播 （数据同步过程）
	2. 崩溃恢复 （Leader 选举）

