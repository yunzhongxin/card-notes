- Metadata
	- Author: superrick
	- lilnk: [An Illustrated Proof of the CAP Theorem](https://mwhittaker.github.io/blog/an_illustrated_proof_of_the_cap_theorem/)
	- Publisher:
	- Date: [[2022-04-12]]
	- Status: 
	- Tag:
- content
consistency: 一致性
availablility：可用性
partition tolerance：分区容错性

### C（一致性）
进行一次写操作后，返回成功，之后再哪个节点访问都应该返回最新值
### A （可用性）
客户端进行请求后服务端必定会返回结果，不可以返回异常
### P （分区容错性）
服务节点间的通信因网络原因可能出现数据同步失败的现象，一般来说分区错误无法避免

### 定理
因分区错误无法避免，所以分布式系统必须支持分区容错性（P），在其余两项（C，A）中只能同时满足一个