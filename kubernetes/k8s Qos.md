- Metadata
	- Author: superrick
	- lilnk:
	- Publisher:
	- Date: [[2022-04-12]]
	- Status: 
	- Tag:
- content
### Qos 概念
Qos(quality of service) 服务质量

### Qos 类型
1. BestEffort （优先级最低）
	1. 没有配置 requests 和 limits 的 pod
2. Burstable
	1. requests 与 limits 不想等的 pod
3. Guaranteed （优先级最高）
	1. requests 与 limits 相等的 pod
	2. cpu 与 memory 都必须有资源配置

### Qos 影响
在节点资源达到限制时 会优先处理 Qos 优先级低的 pod 依次向上

