- Metadata
	- Author: superrick
	- lilnk:
	- Publisher:
	- Date: [[2022-04-26]]
	- Status: 
	- Tag: #cache
- content
### 缓存雪崩
1. 缓存时间区分分散
2. 一些热门数据不设置缓存时间
3. 热门数据预热
4. 数据访问增加限流
5. 服务降级

### 缓存穿透
1. bloom filter
	1. guava
	2. redis bitmap 实现
	3. redisBloom


### redis 缓存淘汰策略
1. noeviction
	1. 无法写入新数据，报错
2. allkeys-lru
	1. 移除最近最少使用的key
3. allkeys-random
	1. 随机一处某个key
4. volatile-lru
	1. 在设置了过期时间的键空间中，移除最近最少使用的key
5. volatile-ttl
	1. 在设置了过期时间的键空间中，移除更早过期时间的key

### 数据一致性
1. 本地缓存
2. 缓存中间件
3. 数据访问模块

### 缓存访问模式
1. cache aside
	1. 先读数据库，再更新cache
	2. 更新数据库后，清楚缓存
	3. 应用程序操作缓存和数据库
2. write through
	1. 写到缓存，缓存写入数据库
3. read through
	1. 尝试从缓存读数据，缓存从数据库家在数据

### 缓存更新删除策略
1. 只更新，不删除
	1. 先更新缓存，在更新数据库
	2. 先更新数据库，在更新缓存
2. 只删除，不更新
	1. 先删除缓存，在更新数据库
	2. 先更新数据库，在删除缓存
3. 缓存延迟双删
	1. 先删缓存，在更新数据库，再删除缓存
4. 删除缓存重试机制
5. 使用 canal binlog 同步数据到 redis
6. 强一致性（分布式事务，分布式一致性算法）