- Metadata
	- Author: superrick
	- lilnk:
	- Publisher:
	- Date: [[2022-04-26]]
	- Status: 
	- Tag: #架构
- content
1. 负载均衡
2. CDN
	1. 静态资源放到CDN，提高各区域的访问速度
	2. 降低系统负载
	3. 防止 DDOS
3. 缓存 [[缓存 (cache)]]
4. 数据库读写分离
5. 文件系统 
	1. FastDFS - 适合小文件
	2. TFS - 适合小文件
	3. HDFS - 适合大文件
6. 搜索
	1. lucene
	2. solr
	3. elasticsearch