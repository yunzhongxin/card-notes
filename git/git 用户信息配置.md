- Metadata
	- Author: superrick
	- lilnk:
	- Publisher:
	- Date: [[2022-04-12]]
	- Status: 
	- Tag: #git
- content
### 查看用户信息
```
$ git config --list
```
### 用户名配置(指定仓库)
```
$ git config user.name "XXXX"
```
### 邮箱配置(指定仓库)
```
$ git config user.email "XX@XX"
```
### 用户名配置(全局)
```
$ git config --global user.name "XXXX"
```
### 邮箱配置(全局)
```
$ git config --global user.email "XX@XX"
```

