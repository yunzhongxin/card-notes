- Metadata
	- Author: superrick
	- lilnk:
	- Publisher:
	- Date: [[2022-04-12]]
	- Status: 
	- Tag:
- content
### 创建本地仓库
```
$ mkdir test
$ cd test
$ git init 
$ touch README.md
$ git add README.md
$ git commit -m "first commit"
```
### http push 方式
```
$ git remote add origin http://XXXX.com/XXX/test.git
$ git push -u origin "master"
```

### ssh push 方式
1. [[生成 ssh rsa 密钥]]
2. 配置公钥
3. 推送
```
$ git remote add origin git@XXXX.com/XXX/test.git
$ git push -u origin "master"
```