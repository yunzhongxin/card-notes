- Metadata
	- Author: superrick
	- lilnk:
	- Publisher:
	- Date: [[2022-04-12]]
	- Status: 
	- Tag: #git
- content
### 目的
正常clone 只会下拉指定分支，git迁移需要迁移所有分支

### clone 整个仓库
```
$ git clone --bare XXX.git
```

### 推送到新的远程仓库
```
$ cd xxx.git 
$ git push --mirror XXXX.git
```
