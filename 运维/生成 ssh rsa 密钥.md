- Metadata
	- Author: superrick
	- lilnk:
	- Publisher:
	- Date: [[2022-04-12]]
	- Status: 
	- Tag: #ssh #rsa
- content

### 生成密钥命令
```
$ ssh-keygen -t rsa -C XXX@XX.com
```

### 查看密钥
```
$ cd ~/.ssh
$ vim id_rsa
$ vim id_rsa.pub
```
id_rsa 是私钥
id_rsa.pub 是公钥