- Metadata
	- Author: superrick
	- lilnk:
	- Publisher:
	- Date: [[2022-04-12]]
	- Status: 
	- Tag: #clickhouse
- content
1. 官网
	https://clickhouse.com
2. 文档
	https://clickhouse.com/docs/en/
3. docker 仓库
	https://hub.docker.com/r/clickhouse/clickhouse-server/
4. 拉取镜像
```
docker pull clickhouse/clickhouse-server
	
```
5. 启动命令
```
docker run -d --name clickhouse-server --ulimit nofile=262144:262144 --volume=/Users/jinqiang/clickhouse:/var/lib/clickhouse yandex/clickhouse-server
```
	