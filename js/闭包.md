- Metadata
	- Author: superrick
	- lilnk:
	- Publisher:
	- Date: [[2022-04-19]]
	- Status: 
	- Tag: #闭包 
- content
### 定义
闭包就是能够读取其他函数内部变量的函数，例如在javascript中，只有函数内部的子函数才能读取局部变量，所以闭包可以理解成“定义在一个函数内部的函数“。在本质上，闭包是将函数内部和函数外部连接起来的桥梁。[闭包](https://baike.baidu.com/item/闭包/10908873?fr=aladdin)

### this 的区别
是在函数调用作用域的this，而不是函数声明作用域内的this

### 参数声明
函数声明作用域内的参数，而不是函数调用作用域内参数

### var,let 区别
var 会挂在到 window 等this对象中，let 则不会