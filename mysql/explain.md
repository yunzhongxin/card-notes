- Metadata
	- Author: superrick
	- lilnk:
	- Publisher:
	- Date: [[2022-04-19]]
	- Status: 
	- Tag: #mysql #explain #sql
- content
1. id
	1. 序号
2. select_type
	1. simple 简单查询，不包括子查询和union
	2. primary 如果查询有任何复杂的子部分，则最外层部分标记为primary
	3. subquery 在select 列表中的子查询（不再 from 子句中）
	4. derived from 子句中的select，会递归执行并将结果放到一个临时表中，服务器内部称其“派生表”
	5. union
	6. union result
3. table
	1. 访问哪个表
4. type （关联类型，访问类型）
	1. ALL 全表扫描
	2. index 也是全表扫描，只是mysql扫描表时按索引次序进行而不是行，主要优点是避免排序，缺点是要承担按索引次序读取整个表的开销，如果 extra 列中看到 “Using index” 说明mysql 正在使用覆盖索引，它只扫描索引数据
	3. range 索引范围查找
	4. ref 指定值索引查找，会有多行匹配
	5. eq_ref 唯一索引查找，mysql 优化很好
	6. const, system mysql 将部分查询优化，使查找变为常量
	7. NULL 优化阶段分解查询语句，在执行阶段不用访问表活着索引
5. possible_keys
	1. 查询可以使用哪些索引
6. key
	1. 查询决定使用哪个索引
7. key_len
	1. 使用索引的字节数
8. ref
	1. 使用的常量或索引
9. rows
	1. mysql 估计为了找到所需的行而要读去的行数
10. Extra
	1. using index
		1. 使用覆盖索引，避免访问表
	2. using where
		1. 检索行后再进行过滤，不是所有带 where子句的查询都会显示 using where
	3. using temporary
		1. 进行排序时会使用临时表
	4. using filesort
		1. 意味着mysql 会对结果使用一个外部索引排序，而不是按索引次序从表里读取行
	5. range checked for each record
		1. 没有好用的索引，新的索引将在连接的每一行上重新估算