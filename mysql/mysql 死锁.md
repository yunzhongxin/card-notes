- Metadata
	- Author: superrick
	- lilnk:
	- Publisher:
	- Date: [[2022-04-21]]
	- Status: #waiting 
	- Tag:
- content
### 产生原因
1. 两个或多个事务在同一资源上相互占用，并请求对方占用的资源，从而导致恶性循环的现象

### 避免死锁
1. 数据库会对死锁进行检测，并立即返回一个错误
2. innoDB 采用的防止死锁的方法是，持有最少行级排他锁的事物会进行回滚