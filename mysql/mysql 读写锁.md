- Metadata
	- Author: superrick
	- lilnk:
	- Publisher:
	- Date: [[2022-04-14]]
	- Status: 
	- Tag: #lock #mysql
- content
1. 共享锁（shared lock），排他锁 (exclusive lock)
2. 读锁 (read lock)，写锁 (write lock)
3. 读锁有更高的并发优先度，读锁可能会被插入到同时等待的写锁前面