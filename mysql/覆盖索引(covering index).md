- Metadata
	- Author: superrick
	- lilnk:
	- Publisher:
	- Date: [[2022-04-16]]
	- Status: 
	- Tag: #mysql #索引
- content
1. 索引的叶子节点中已经包含要查询的数据，就不需要回表查询
2. 如果一个索引包含所有需要查询的字段值，称之为覆盖索引