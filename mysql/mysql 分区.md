- Metadata
	- Author: superrick
	- lilnk:
	- Publisher:
	- Date: [[2022-04-16]]
	- Status: 
	- Tag: #mysql #分区
- content
1. 分区表是一个独立的逻辑表，但是底层由多个物理表组成，实现分区的代码实际上是对一组地层表的句柄对象（Handler Object）的封装。对分区表的请求，都会通过举兵对象转化成对存储引擎的借口调用。所以分区对于sql 层来说是一个完全封装底层实现的黑盒子。
2. 建表sql
```
CREATE TABLE sales (
	order_date DATETIME NOT NULL,
	-- Othercolumns omitted
	) ENGINE=InnoDB PARTITION BY RANGE(YEAR(order_date))(
		PARTITION p_2010 VALUES LESS THAN (2010),
		PARTITION p_2011 VALUES LESS THAN (2011),
		PARTITION p_2012 VALUES LESS THAN (2012),
		PARTITION p_catchall VALUES LESS THAN MAXVALUE ); 
```