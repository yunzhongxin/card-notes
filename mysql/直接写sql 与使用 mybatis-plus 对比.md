- Metadata
	- Author: superrick
	- lilnk:
	- Publisher:
	- Date: [[2022-04-14]]
	- Status: 
	- Tag: #mysql #sql #索引
- content
### 直接使用sql
1. 写起来麻烦
2. 可以针对where字段进行排序，迎合组合索引([[索引的好处]])的顺序

### 使用mybatis-plus
1. 编码方便
2. 一些查询条件的顺序无法确保，可能影响组合索引([[索引的好处]])的性能