- Metadata
	- Author: superrick
	- lilnk:
	- Publisher:
	- Date: [[2022-04-21]]
	- Status: 
	- Tag: #mysql #事务
- content
### 事务隔离级别
1. read uncommited (未提交读)
	1. 事务中修改，即使没有提交，对其他事务也都是可见的
	2. 事务可以读取到未提交的数据，被称为脏读（dirty read）
2. read committed （提交读）
	1. 大多数数据库的隔离级别都是这个（mysql 不是）
	2. 一个事务开始时，只能看见已经提交的事务所做的修改
	3. 这个级别也叫做不可重复读（nonrepeatable read），因为两次执行同样的查询，可能得到不一样的结果
3. repeatable read （可重复读）
	1. 该级别保证了在一个事物中多次读取同样的记录的结果是一致的
	2. 解决了脏读问题，但是有幻读（phantom read）的问题，记录行数可能有变化
	3. InnoDB 和 XtraDB 存储引擎通过[[多版本并发控制（MVCC）]]解决了幻读的问题
	4. 可重复度是mysql 默认事务隔离级别
4. serializable （可串行化）
	1. 通过强制事务串行执行，避免了幻读问题
	2. 会在读取的每一行数据加上锁，所以可能导致大量的超时和锁争用的问题
	3. 会导致查询很慢

### [[mysql 死锁]]

### 手动开启事务

```
STARTTRANSACTION;
...
COMMIT;
```

### 自动提交 （autocommit）
1. 自动提交启用时，默认会将 每条sql 语句加上事务
2. 确认自动提交是否打开,1 或者 on 为打开状态
```
show variables like 'autocommit';
```
