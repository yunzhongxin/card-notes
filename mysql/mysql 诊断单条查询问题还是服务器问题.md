- Metadata
	- Author: superrick
	- lilnk:
	- Publisher:
	- Date: [[2022-04-14]]
	- Status: 
	- Tag: #mysql #诊断
- content
### 使用 SHOW GLOBAL STATUS
每秒执行一次
```
$ mysqladmin ext -i1 | awk' 
/Queries/{q=$4qp;qp=$4} 
/Threads_connected/{tc=$4}
/Threads_running/{printf"%5d%5d%5d\n",q,t

施瓦茨(Baron Schwartz),扎伊采夫(Peter Zaitsev),特卡琴科(Vadim Tkachenko). 高性能MySQL(第3版) (Chinese Edition) (Kindle Locations 2414-2416). 电子工业出版社. Kindle Edition. 
```

### SHOW PROCESSLIST
查看查询中的线程状态

### 使用查询日志
开启慢查询日志
