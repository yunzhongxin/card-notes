- Metadata
	- Author: superrick
	- lilnk: [什么是 MySQL 的“回表”？](https://baijiahao.baidu.com/s?id=1721822869837734596&wfr=spider&for=pc)
	- Publisher:
	- Date: [[2022-04-16]]
	- Status: 
	- Tag: #mysql #索引
- content
### 回表
1. 使用非聚簇索引进行查找，并在叶子结点中查找到主键值，再通过聚簇索引查找对应主键的数据，这一过程称为回表

### 回表发生情况
1. 使用非[[聚簇索引(clustered index)]]查询
2. 查询字段没有[[覆盖索引(covering index)]]

### 避免回表
1. 使用聚簇索引查找
2. 使用覆盖索引查找