- Metadata
	- Author: superrick
	- lilnk:
	- Publisher:
	- Date: [[2022-04-29]]
	- Status: 
	- Tag: #zookeeper #命令
- content
1. 启动服务
```
$ zkServer.sh
```
2. 使用客户端连接
```
$ zkCli.sh -server localhost:2189
```
3. 创建目录
```
$ create /goods Java
```
4. 查看目录
```
$ ls /
```
5. 查看目录
```
$ get /goods
```
6. 设置目录
```
$ set /goods zookeeper
```
7. 删除目录
```
$ delete /goods
```
8. 生成有序目录
```
$ create -s /seq
```
9. 临时借点
```
$ create -e /temp
```