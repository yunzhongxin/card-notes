- Metadata
	- Author: superrick
	- lilnk:
	- Publisher:
	- Date: [[2022-04-12]]
	- Status: 
	- Tag: #docker #iptables #telnet
- content
### 现象
mysql 运行在docker 容器中，同服务器可以通过 [[telnet]] 进行访问，但其他服务器及外网无法链接置顶端口 3306

### 停止docker 容器后 无法正常启动

### 需要在iptables 增加配置
