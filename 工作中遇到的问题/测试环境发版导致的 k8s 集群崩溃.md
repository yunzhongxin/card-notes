- Metadata
	- Author: superrick
	- lilnk:
	- Publisher:
	- Date: [[2022-04-12]]
	- Status: 
	- Tag: #k8s #qos #pod
- content
### 现象
k8s 中的jenkins 部署 vuejs 项目，会生成 nodejs pod 进行编译，把便已完成的文件发送到新建的 nginx pod 进行发布前端页面，但是会产生内存不足导致 大部分pod 崩溃，可能包括 ApiService 导致 lens 断开链接

### 推测
因核心组建的 [[k8s Qos]] 配置问题导致，[[k8s 核心组建]]的 Qos 过低导致 整个k8s 集群崩溃

