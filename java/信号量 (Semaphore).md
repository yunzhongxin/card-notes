- Metadata
	- Author: superrick
	- lilnk:
	- Publisher:
	- Date: [[2022-04-24]]
	- Status: 
	- Tag: #thread #java
- content
### Counting Semaphore (计数信号量)
1. 用来控制访问某个资源的操作数量
2. Semephore::acquire() 获取计数权限
3. Semephore::release() 释放计数
4. 以下是有边界的 HashSet 实现
```
public class BoundedHashSet<T> {  
      
	private final Set<T> set;  
	private final Semaphore sem;  
	
	public BoundedHashSet(int bound) {  
		this.set = Collections.synchronizedSet(new HashSet<>());  
		sem = new Semaphore(bound);  
	}  
	  
	public boolean add (T o) throws InterruptedException {  
		sem.acquire();  
		boolean wasAdded = false;  
		try {  
			wasAdded = set.add(o);  
			return wasAdded;  
		} finally {  
			if (!wasAdded) {  
				sem.release();  
			}  
		}  
	}  
	  
	public boolean remove (Object o) {  
		boolean wasRemoved = set.remove(o);  
		if (wasRemoved) {  
			sem.release();  
		}  
		return wasRemoved;  
	}  
}
```