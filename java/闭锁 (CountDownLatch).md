- Metadata
	- Author: superrick
	- lilnk:
	- Publisher:
	- Date: [[2022-04-24]]
	- Status: 
	- Tag: #thread #java
- content

1. 闭锁是一个同步工具，可以延迟线程的进度直到其到达终止状态
2. 希望多个线程同时运行或者所有线程全部结束完成后再执行其他逻辑时可以使用

### CountDownLatch
1. CountDownLatch 是一个灵活的闭锁实现
2. 以下是并发任务计时器实现
```
public static long countDownLatchTest(int threadCount, Runnable task) throws InterruptedException {
	CountDownLatch startGate = new CountDownLatch(1);  
	CountDownLatch endGate = new CountDownLatch(threadCount);  
  
	for (int i = 0; i < threadCount ; i++) {  
		Thread t = new Thread(() -> {  
			 try {  
				 startGate.await();  
				 task.run();  
			 } catch (Exception e) {  
				 throw new RuntimeException(e);  
			 } finally {  
				 endGate.countDown();  
			 }  
		 });  
		 t.start();  
	}  
  
	long start = System.currentTimeMillis();  
	startGate.countDown();  
	endGate.await();  
	long end = System.currentTimeMillis();  
	return end - start;  
}
```
### FutureTask
1. futureTask 也可以用做闭锁
2. 计算通过 Callable，相当于有返回结果的 Runnable
3. 调用 FutureTask::get(), 线程运行结束会返回结果，否则将阻塞