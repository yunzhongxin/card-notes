- Metadata
	- Author: superrick
	- lilnk:
	- Publisher:
	- Date: [[2022-04-20]]
	- Status: 
	- Tag:
- content
### java memory model
1. 主内存(Main memory)
	1. 堆内存基本对应主内存，没有线程限制
	2. 工作内存中的修改会同步到主内存中
2. 工作内存(Working memory)
	1. 栈内存基本对应工作内存，是线程独享内存
	2. 赋值，取值都会在 工作内从中进行
3. volatile
	1. 在一个线程对volatile 变量进行修改后，其他线程可以看到值的变化，可见行