- Metadata
	- Author: superrick
	- lilnk:
	- Publisher:
	- Date: [[2022-04-20]]
	- Status:  
	- Tag: #thread #java #threaddump
- content
### 线程状态转化
1. new（新建）
	1. 创建后尚未启动的线程
2. runnable（运行）
	1. 包括系统状态中的 running 和 ready，此状态的线程在执行或等待cpu分配执行时间
3. waiting（无期限等待）
	1. 处于此状态的线程不会被分配cpu执行时间，他们要等待其他线程显式唤醒
	2. 以下方法会使线程处于此状态
	3. 没有设置timeout 的 Object::wait()
	4. 没有设置timeout 的 Thread::join()
	5. LockSupport::park()
4. timed waiting（限期等待）
	1. 处于此状态的线程也不会被分配cpu执行时间，但无需其他线程唤醒，再一定时间之后会由系统自动唤醒
	2. 以下方法会使线程处于此状态
	3. Thread::sleep()
	4. 设置了timeout 的 Object::wait()
	5. 设置了timeout 的 Thread::join()
	6. LockSupport::parkNanos()
	7. LockSupport::parkUntil
5. blocked（阻塞）
	1. 阻塞状态与等待状态的区别是，他在等待一个排他锁，这个事件将在另外一个线程放弃这个锁的时候发生
6. terminated
	1. 线程已经结束执行

### 状态转移
![[effdb02faf1f8af0a1c658aa40c1b8f5.jpeg]]

### threaddump 分析



