- Metadata
	- Author: superrick
	- lilnk:
	- Publisher:
	- Date: [[2022-04-24]]
	- Status: 
	- Tag: #thread #java
- content
### Barrier
所有线程等待某个事件触发并执行完成后

### CyclicBarrier
```
public static void cyclicBarrierTest(int threadCount) {  
    CyclicBarrier barrier = new CyclicBarrier(threadCount, () -> {  
        try {  
            Thread.sleep(1000);  
		 } catch (InterruptedException e) {  
				throw new RuntimeException(e);  
		 }  
	        System.out.println("finish");  
		 });  
		 Random random = new Random();  
	  
	 for (int i = 0; i < threadCount; i++) {  
	        Thread t = new Thread(() -> {  
		 try {  
			 barrier.await();  
			 System.out.println(random.nextInt(1000));  
		 } catch (InterruptedException e) {  
			 throw new RuntimeException(e);  
		 } catch (BrokenBarrierException e) {  
			 throw new RuntimeException(e);  
		 }  
		 });  
		 t.start();  
	 }  
  
}
```

### Exchanger
1. 也可以用Exchanger 实现栅栏