- Metadata
	- Author: superrick
	- lilnk:
	- Publisher:
	- Date: [[2022-04-25]]
	- Status: 
	- Tag: #java #thread 
- content
### Executors
1. 线程池工具类
2. 工厂静态方法
	1. newFixedThreadPool
		1. 固定长度线程池
	2. newCachedThreadPool
		1. 线程池的线程数量可以变多变少
	3. newSingleThreadPool
		1. 单线程线程池
	4. newScheduledThreadPool
		1. 可以延迟或定时执行的线程池
### ExecutorSerivce 生命周期
1. 运行
2. 关闭
3. 已终止
