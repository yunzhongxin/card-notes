- Metadata
	- Author: superrick
	- lilnk:
	- Publisher:
	- Date: [[2022-04-29]]
	- Status: 
	- Tag: #分布式 #ID
- content

### 要求
1. 全局唯一
2. 高性能
3. 高可用
4. 好接入
5. 趋势增长

### 方式
1. UUID
2. 数据库自增
3. 雪花算法
4. redis 自增key
	1. 业务ID + 时间精确到毫秒 + 三位随机数